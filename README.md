# README #

This is a SDL Shader Sample

### What is this repository for? ###

* SDL Shader Sample

### How do I get set up? ###

* Clone repository
* Download Development Libraries for  Windows 32-bit OR 64-bit
* Set Environment Variables using this guide https://support.microsoft.com/en-us/kb/310519
* Download **SDL** from https://www.libsdl.org/download-2.0.php
	* Set environment variable for SDL
	* Alternatively `SET SDL_SDK="C:\####\SDL_###"` directory location
* Download **SDL Image** from https://www.libsdl.org/projects/SDL_image/	
	* Set environment variable for SDL Image
	* Alternatively `SET SDL_IMAGE_SDK="C:\Users\#####\####\SDL_Image_###"` directory location
* To check environment variable is set correctly open a command prompt and type `echo %GLEW_SDK%` the path to glew sdk should be show.
* Select a project default target `x86` when running executable
* If the project builds but does not `xcopy` the required dll's try moving your project to a directory you have full access to, see http://tinyurl.com/SFMLStarter for a guide on post build events.
* Alternatively set the Environment Variable in Configuration Properties | Debugging | Environment to `PATH=%PATH%;$(SDL_IMAGE_SDK)\lib\x86;$(SDL_SDK)\lib\x86` this will ensure DLL's are discoverable when running in debug mode without copying the DLL's to the executable directory

### Cloning Repository ###
* Run GitBash and type the Follow commands into GitBash

* Check Present Working Directory `pwd`

* Change to the C drive or other drive `cd c:`

* Make a projects Directory `mkdir projects`

* Change to the projects directory by `cd projects`

* Clone the project `git clone https://MuddyGames@bitbucket.org/MuddyGames/sdl_shaders.git`

* Change to the project directory `cd projects sdl_dynamic_camera.git`

* List files that were downloaded `ls`

### Who do I talk to? ###

* philip.bourke@itcarlow.ie