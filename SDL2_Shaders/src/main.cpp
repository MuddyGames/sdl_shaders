#include <iostream>
#include <string>

#include <Debug.h>

#include <SDL.h>
#undef main

#include <SDL_image.h>

#include <SDL_OpenGL_Impl.h>



#define SCREEN_WIDTH		800
#define SCREEN_HEIGHT		600
#define LEVEL_WIDTH			1600
#define LEVEL_HEIGHT		1200

using namespace std;


// Toggle fullscreen
void fullScreenToggle(SDL_Window* Window) {
	Uint32 FullscreenFlag = SDL_WINDOW_FULLSCREEN;
	bool IsFullscreen = SDL_GetWindowFlags(Window) & FullscreenFlag;
	SDL_SetWindowFullscreen(Window, IsFullscreen ? 0 : FullscreenFlag);
	SDL_ShowCursor(IsFullscreen);
}

int main(int argc, char* args[]) {

	SDL_Window* window = NULL;
	SDL_Renderer* renderer = NULL;

	bool exit = false;

	//Initialize SDL
	if (SDL_Init(SDL_INIT_EVERYTHING | SDL_VIDEO_OPENGL) < 0) {
		fprintf(stderr, "Error Initializing %s\n", SDL_GetError());
		return 1;
	}

	window = SDL_CreateWindow(
		"SDL Shader",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		SCREEN_WIDTH,
		SCREEN_HEIGHT,
		SDL_WINDOW_OPENGL
	);


	if (window == NULL) {
		fprintf(stderr, "could not create window: %s\n", SDL_GetError());
		return 1;
	}

	// OpenGL Rendering
	SDL_SetHint(SDL_HINT_RENDER_DRIVER, "opengl");

	renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_TARGETTEXTURE);

	if (renderer == NULL)
	{
		fprintf(stderr, "could not create renderer: %s\n", SDL_GetError());
		return 1;
	}

	SDL_RendererInfo rendererInfo;
	SDL_GetRendererInfo(renderer, &rendererInfo);

#if defined DEBUG
	DEBUG_MSG("Graphics Library:" + std::string(rendererInfo.name));
	DEBUG_MSG("Supported Rendering:" + std::to_string(rendererInfo.flags));
	DEBUG_MSG("Number of Texture Formats:" + std::to_string(rendererInfo.num_texture_formats));
	DEBUG_MSG("Texture Formats");
	for (int i = 0; i < sizeof(rendererInfo.texture_formats); i++) {
		DEBUG_MSG(std::to_string(rendererInfo.texture_formats[i]));
	};
	DEBUG_MSG("Max Texture Width:" + std::to_string(rendererInfo.max_texture_width));
	DEBUG_MSG("Max Texture Height:" + std::to_string(rendererInfo.max_texture_height));
#endif

	if (SDL_OpenGL_Ext_Init() < 0) {
		fprintf(stderr, "Error Initializing %s\n", SDL_GetError());
		return 1;
	}

	// ********************************************************************
	// Setting Up Shaders
	// ********************************************************************
#if defined DEBUG
	DEBUG_MSG("Shader Setup and Compilation");
#endif
	GLuint	vfx1;			//Program ID		

	
#if defined DEBUG
	DEBUG_MSG("Setting Up Shaders");
#endif

	vfx1 = SDL_OpenGL_CompileProgram("./res/shaders/std.vertex","./res/shaders/curve.fragment");


#if defined DEBUG
	DEBUG_MSG("Render Copying Game Art Assets");
#endif

	SDL_Surface* surface_tile1 = IMG_Load("./res/tiles/tile_1.png");
	SDL_Texture* texture_tile1 = SDL_CreateTextureFromSurface(renderer, surface_tile1);
	SDL_FreeSurface(surface_tile1);

	SDL_Surface* surface_tile2 = IMG_Load("./res/tiles/tile_2.png");
	SDL_Texture* texture_tile2 = SDL_CreateTextureFromSurface(renderer, surface_tile2);
	SDL_FreeSurface(surface_tile2);

	SDL_Surface* surface_tile3 = IMG_Load("./res/tiles/tile_3.png");
	SDL_Texture* texture_tile3 = SDL_CreateTextureFromSurface(renderer, surface_tile3);
	SDL_FreeSurface(surface_tile3);

	SDL_Surface* surface_tile4 = IMG_Load("./res/tiles/tile_4.png");
	SDL_Texture* texture_tile4 = SDL_CreateTextureFromSurface(renderer, surface_tile4);
	SDL_FreeSurface(surface_tile4);


	SDL_Surface* surface_back = IMG_Load("./res/screen/background.png");
	SDL_Texture* texture_back = SDL_CreateTextureFromSurface(renderer, surface_back);
	SDL_FreeSurface(surface_back);

	SDL_Surface* surface_fore = IMG_Load("./res/screen/foreground.png");
	SDL_Texture* texture_fore = SDL_CreateTextureFromSurface(renderer, surface_fore);
	SDL_FreeSurface(surface_fore);

	SDL_Surface* surface_character = IMG_Load("./res/characters/minecraft.png");
	SDL_Texture* texture_character = SDL_CreateTextureFromSurface(renderer, surface_character);
	SDL_FreeSurface(surface_character);

#if defined DEBUG
	DEBUG_MSG("Setting up a Frame Texture to copy Game Art Assets to");
#endif
	//Target Texture for Frame note SDL_TEXTUREACCESS_TARGET
	SDL_Texture* level_frame = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888,
		SDL_TEXTUREACCESS_TARGET, LEVEL_WIDTH, LEVEL_HEIGHT);

	SDL_Texture* screen_frame = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_RGBA8888,
		SDL_TEXTUREACCESS_TARGET, SCREEN_WIDTH, SCREEN_HEIGHT);


	SDL_Rect* camera = new SDL_Rect();
	camera->x = 0;
	camera->y = 0;
	camera->w = SCREEN_WIDTH;
	camera->h = SCREEN_HEIGHT;

	// ******************************************************************

	SDL_OpenGL_Init_Orho(SCREEN_WIDTH, SCREEN_HEIGHT);

	bool Exit = false;
	while(!Exit)
	{
		SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);

#if defined DEBUG
		DEBUG_MSG("RenderCopy Game Art Assets");
#endif

		SDL_SetRenderTarget(renderer, level_frame);
		SDL_RenderClear(renderer);


		SDL_Rect* temp_tile = new SDL_Rect();

		temp_tile->w = 64 * 3;
		temp_tile->h = 64 * 3;

		temp_tile->x = camera->x;
		temp_tile->y = camera->y;
		

		SDL_RenderCopy(renderer, texture_back, NULL, NULL);
		SDL_RenderCopy(renderer, texture_fore, NULL, NULL);
		SDL_RenderCopy(renderer, texture_character, NULL, NULL);

		SDL_RenderCopy(renderer, texture_tile1, NULL, temp_tile);

		temp_tile->x = camera->w - temp_tile->w;
		temp_tile->y = camera->y;

		SDL_RenderCopy(renderer, texture_tile2, NULL, temp_tile);

		temp_tile->x = camera->x;
		temp_tile->y = camera->h - temp_tile->h;

		SDL_RenderCopy(renderer, texture_tile3, NULL, temp_tile);

		temp_tile->x = camera->w - temp_tile->w;
		temp_tile->y = camera->h - temp_tile->h;

		SDL_RenderCopy(renderer, texture_tile1, NULL, temp_tile);

		delete(temp_tile);

#if defined DEBUG
		DEBUG_MSG("Call SDL_OpenGL_Render");
#endif

		SDL_OpenGL_Apply_Frame_FX(window, renderer, level_frame, screen_frame, camera, vfx1);

		SDL_GL_SwapWindow(window);

		{SDL_Event Event; 
			SDL_WaitEvent(&Event);
			if (Event.type == SDL_KEYDOWN) {
				switch (Event.key.keysym.sym) {
				case SDLK_f: fullScreenToggle(window); break;
				case SDLK_q: Exit = true; break;
				}
			}
		}
	}

	glUseProgram(NULL);

	SDL_DestroyTexture(level_frame);

	SDL_DestroyTexture(texture_back);
	SDL_DestroyTexture(texture_fore);

	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
	SDL_Quit();
}



